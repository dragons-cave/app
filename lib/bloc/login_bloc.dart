import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:home_app/bloc/auth_bloc.dart';
import 'package:home_app/services/user_service.dart';
import 'package:meta/meta.dart';

// STATE
abstract class LoginState extends Equatable {
  LoginState([List props = const []]) : super(props);
}

class LoginInitial extends LoginState {
  @override
  String toString() => 'LoginInitial';
}

class LoginLoading extends LoginState {
  @override
  String toString() => 'LoginLoading';
}

class LoginFailure extends LoginState {
  final Map<String, dynamic> errors;

  LoginFailure({@required this.errors}) : super([errors]);

  @override
  String toString() => 'LoginFailure { error: ${errors.toString()} }';
}

// EVENT
abstract class LoginEvent extends Equatable {
  LoginEvent([List props = const []]) : super(props);
}

class LoginButtonPressed extends LoginEvent {
  final String email;
  final String password;

  LoginButtonPressed({
    @required this.email,
    @required this.password,
  }) : super([email, password]);

  @override
  String toString() =>
      'LoginButtonPressed { email: $email, password: $password }';
}

//Bloc
class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final UserService userService = new UserService();
  final AuthenticationBloc authenticationBloc;

  LoginBloc({
    @required this.authenticationBloc,
  }) : assert(authenticationBloc != null);

  @override
  LoginState get initialState => LoginInitial();

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is LoginButtonPressed) {
      yield LoginLoading();

      try {
        final Response response = await userService.authenticate(
          email: event.email,
          password: event.password,
        );

        authenticationBloc.loggedIn(response.data['token']);

        yield LoginInitial();
      } on DioError catch (error) {
        yield LoginFailure(errors: error.response.data['errors']);
      } catch(error){
        yield LoginFailure(errors: {'error': error.toString() });
      }
    }
  }

  void login(email, password) {
    dispatch(LoginButtonPressed(email: email, password: password));
  }
}
