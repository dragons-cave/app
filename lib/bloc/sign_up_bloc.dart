import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:home_app/bloc/auth_bloc.dart';
import 'package:home_app/services/user_service.dart';
import 'package:meta/meta.dart';

// STATE
abstract class SignUpState extends Equatable {
  SignUpState([List props = const []]) : super(props);
}

class SignUpInitial extends SignUpState {
  @override
  String toString() => 'SignUpInitial';
}

class SignUpLoading extends SignUpState {
  @override
  String toString() => 'SignUpLoading';
}

class SignUpFailure extends SignUpState {
  final Map<String, dynamic> errors;

  SignUpFailure({@required this.errors}) : super([errors]);

  @override
  String toString() => 'SignUpFailure { error: $errors }';
}

// EVENT
abstract class SignUpEvent extends Equatable {
  SignUpEvent([List props = const []]) : super(props);
}

class SignUpButtonPressed extends SignUpEvent {
  final String name;
  final String email;
  final String password;

  SignUpButtonPressed({
    @required this.name,
    @required this.email,
    @required this.password,
  }) : super([name, email, password]);

  @override
  String toString() =>
      'SignUpButtonPressed { name: $name, email: $email, password: $password }';
}

//Bloc
class SignUpBloc extends Bloc<SignUpEvent, SignUpState> {
  final UserService userService = new UserService();
  final AuthenticationBloc authenticationBloc;

  SignUpBloc({
    @required this.authenticationBloc,
  }) : assert(authenticationBloc != null);

  @override
  SignUpState get initialState => SignUpInitial();

  @override
  Stream<SignUpState> mapEventToState(SignUpEvent event) async* {
    if (event is SignUpButtonPressed) {
      yield SignUpLoading();

      try {
        Response response = await userService.signUp(
          email: event.email,
          name: event.name,
          password: event.password,
        );

        authenticationBloc.loggedIn(response.data['token']);
        yield SignUpInitial();
      } on DioError catch (error) {
        yield SignUpFailure(errors: error.response.data['errors']);
      } catch(error){
        yield SignUpFailure(errors: {'error': error.toString() });
      }
    }
  }

  void signUp(name, email, password) {
    dispatch(SignUpButtonPressed(name: name, email: email, password: password));
  }
}
