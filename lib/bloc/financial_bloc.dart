import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:home_app/services/financial_service.dart';
import 'package:meta/meta.dart';

// STATE
abstract class FinancialState extends Equatable {
  FinancialState([List props = const []]) : super(props);
}

class FinancialInitial extends FinancialState {
  @override
  String toString() => 'FinancialInitial';
}

class FinancialLoading extends FinancialState {
  @override
  String toString() => 'FinancialLoading';
}

class FinancialLoaded extends FinancialState {
  final List bills;

  FinancialLoaded({@required this.bills});

  @override
  String toString() => 'FinancialLoaded';
}

class FinancialFailure extends FinancialState {
  final String error;

  FinancialFailure({@required this.error}) : super([error]);

  @override
  String toString() => 'FinancialFailure { error: $error }';
}

// EVENT
abstract class FinancialEvent extends Equatable {
  FinancialEvent([List props = const []]) : super(props);
}

class FinancialTabOpened extends FinancialEvent {
  final int page;

  FinancialTabOpened({
    this.page = 1,
  }) : super([page]);

  @override
  String toString() => 'FinancialTabOpened { page: $page}';
}

//Bloc
class FinancialBloc extends Bloc<FinancialEvent, FinancialState> {
  final FinancialService financialService = new FinancialService();

  @override
  FinancialState get initialState => FinancialInitial();

  @override
  Stream<FinancialState> mapEventToState(FinancialEvent event) async* {
    if (event is FinancialTabOpened) {
      yield FinancialLoading();

      try {
        final bills = await financialService.list(page: event.page);

        yield FinancialLoaded(bills: bills);
      } catch (error) {
        yield FinancialFailure(error: error.toString());
      }
    }
  }
}
