import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:home_app/models/user_model.dart';
import 'package:home_app/services/user_service.dart';
import 'package:meta/meta.dart';

// STATE
abstract class AuthenticationState extends Equatable {}

class AuthenticationUninitialized extends AuthenticationState {
  @override
  String toString() => 'AuthenticationUninitialized';
}

class AuthenticationAuthenticated extends AuthenticationState {
  @override
  String toString() => 'AuthenticationAuthenticated';
}

class AuthenticationUnauthenticated extends AuthenticationState {
  @override
  String toString() => 'AuthenticationUnauthenticated';
}

class AuthenticationLoading extends AuthenticationState {
  @override
  String toString() => 'AuthenticationLoading';
}

// EVENTS
abstract class AuthenticationEvent extends Equatable {
  AuthenticationEvent([List props = const []]) : super(props);
}

class AppStarted extends AuthenticationEvent {
  @override
  String toString() => 'AppStarted';
}

class LoggedIn extends AuthenticationEvent {
  final String token;

  LoggedIn({@required this.token}) : super([token]);

  @override
  String toString() => 'LoggedIn { token: $token }';
}

class LoggedOut extends AuthenticationEvent {
  @override
  String toString() => 'LoggedOut';
}

//Bloc

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  final UserService userService = UserService();

  UserModel user;

  AuthenticationBloc();

  @override
  AuthenticationState get initialState => AuthenticationUninitialized();

  @override
  Stream<AuthenticationState> mapEventToState(
      AuthenticationEvent event) async* {
    if (event is AppStarted) {
      final bool hasToken = await userService.hasToken();

      if (!hasToken) {
        yield AuthenticationUnauthenticated();
        return;
      }

      try {
        yield AuthenticationLoading();

        this.user = await userService.getUser();

        yield AuthenticationAuthenticated();
        return;
      } catch (e) {
        yield AuthenticationUnauthenticated();
        return;
      }
    }

    if (event is LoggedIn) {
      await userService.persistToken(event.token);
      yield AuthenticationAuthenticated();
      return;
    }

    if (event is LoggedOut) {
      await userService.deleteToken();
      yield AuthenticationUnauthenticated();
    }
  }

  void loggedOut() {
    dispatch(LoggedOut());
  }

  void loggedIn(token) {
    dispatch(LoggedIn(token: token));
  }
}
