import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:home_app/models/bill_event_model.dart';
import 'package:home_app/models/bill_model.dart';
import 'package:home_app/services/financial_service.dart';
import 'package:meta/meta.dart';

// STATE
abstract class BillState extends Equatable {
  BillModel billModel;

  BillState([List props = const []]) : super(props);
}

class BillInitial extends BillState {
  BillModel billModel;

  BillInitial({@required this.billModel}) : super([billModel]);

  @override
  String toString() => 'BillInitial';
}

class BillCreated extends BillState {
  BillModel billModel;

  BillCreated({@required this.billModel}) : super([billModel]);

  @override
  String toString() => 'BillCreated';
}

class BillEventsLoaded extends BillState {
  BillModel billModel;

  BillEventsLoaded({@required this.billModel}) : super([billModel]);

  @override
  String toString() => 'BillLoaded';
}

class BillFailure extends BillState {
  final Map<String, dynamic> errors;

  BillFailure({@required this.errors});

  @override
  String toString() => 'BillFailure { error: $errors }';
}

class BillEventsFailure extends BillState {
  final Map<String, dynamic> errors;

  BillEventsFailure({@required this.errors});

  @override
  String toString() => 'BillEventsFailure { error: $errors }';
}

class BillEventsLoading extends BillState {
  BillModel billModel;

  BillEventsLoading({@required this.billModel}) : super([billModel]);

  @override
  String toString() => 'BillEventsLoading';
}

class BillLoading extends BillState {
  @override
  String toString() => 'BillLoading';
}

// EVENT
abstract class BillEvent extends Equatable {
  BillEvent([List props = const []]) : super(props);
}

class LoadBillEvents extends BillEvent {
  final int page;

  LoadBillEvents({
    this.page = 1,
  }) : super([page]);

  @override
  String toString() => 'LoadBillEvents { page: $page}';
}

class CreateBillButtonPressed extends BillEvent {
  final Map<String, dynamic> data;

  CreateBillButtonPressed({
    @required this.data,
  }) : super([data]);

  @override
  String toString() => 'CreateBillButtonPressed { data: $data}';
}

class SendingMessage extends BillEvent {
  final String text;

  SendingMessage({
    @required this.text,
  }) : super([text]);

  @override
  String toString() => 'SendingMessage { text: $text}';
}

class NewMessage extends BillEvent {
  final BillEventModel billEventModel;

  NewMessage({
    @required this.billEventModel,
  }) : super([billEventModel]);

  @override
  String toString() => 'NewMessage { billEventModel: $billEventModel}';
}


//Bloc
class BillBloc extends Bloc<BillEvent, BillState> {
  final FinancialService financialService = new FinancialService();
  final BillModel billModel;

  BillBloc({this.billModel});

  @override
  BillState get initialState => BillInitial(billModel: billModel);

  @override
  Stream<BillState> mapEventToState(BillEvent event) async* {
    print(event.toString());
    if (event is CreateBillButtonPressed) {
      yield BillLoading();

      try {
        BillModel billModel = await financialService.createBill(event.data);
        yield BillCreated(billModel: billModel);
      } on DioError catch (error) {
        yield BillFailure(errors: error.response.data['errors']);
      } catch (error) {
        yield BillFailure(errors: {'error': error.toString()});
      }
    }

    if (event is LoadBillEvents) {
      yield BillEventsLoading(billModel: billModel);

      try {
        List<BillEventModel> billEvents =
        await financialService.listEventsBill(billModel.id, page: event.page);

        if (event.page == 1) {
          billModel.billEvents = billEvents;
        } else {
          billModel.billEvents.addAll(billEvents);
        }

        yield BillInitial(billModel: billModel);
      } catch (error) {
        yield BillFailure(errors: {'error': error.toString()});
      }
    }

    if (event is SendingMessage) {
      yield BillEventsLoading(billModel: billModel);

      billModel.billEvents.insert(0, BillEventModel.fromMessage(event.text));

      yield BillInitial(billModel: billModel);

      try {
        BillEventModel billEventModel =
        await financialService.createBillEvent(billModel, event.text);

        billModel.billEvents[0] = billEventModel;

        yield BillInitial(billModel: billModel);
      } catch (error) {
        billModel.billEvents.removeAt(0);
        yield BillInitial(billModel: billModel);
        print(error.toString());
      }
    }

    if(event is NewMessage){
      yield BillEventsLoading(billModel: billModel);
      billModel.billEvents.insert(0, event.billEventModel);
      yield BillInitial(billModel: billModel);
    }
  }

  void sendMessage(text) {
    dispatch(SendingMessage(text: text));
  }

  addEventBySocket(dynamic data) {
    dispatch(NewMessage(billEventModel: BillEventModel.fromJson(data)));
  }
}
