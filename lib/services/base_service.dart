import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';

class BaseService {
  static const BASE_URL = 'https://home-backend-app.herokuapp.com';
  Dio dio;

  BaseService() {
    BaseOptions options = new BaseOptions(
      baseUrl: BASE_URL,
    );

    dio = new Dio(options);

    dio.interceptors.add(
      InterceptorsWrapper(
        onRequest: (RequestOptions options) async {
          SharedPreferences prefs = await SharedPreferences.getInstance();
          String token = prefs.getString('token');
          if (token != null) {
            options.headers['Authorization'] = "Bearer $token";
          }
          return options;
        },
        onResponse: (Response response) async {
          String token = response.headers.value('refreshToken');

          if (token != null) {
            SharedPreferences prefs = await SharedPreferences.getInstance();
            await prefs.setString('token', token);
          }

          return response;
        },
        onError: (DioError e) async {
          if(e.response.statusCode == 401){

          }
          return e;
        },
      ),
    );
  }
}
