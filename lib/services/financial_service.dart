import 'package:dio/dio.dart';
import 'package:home_app/models/bill_event_model.dart';
import 'package:home_app/models/bill_model.dart';
import 'package:home_app/services/base_service.dart';

class FinancialService extends BaseService {
  Future<List<BillModel>> list({
    int page = 1,
  }) async {
    Response response =
        await dio.get('/bill', queryParameters: {'page': page});

    List<dynamic> json = response.data;

    return json.map((item) => BillModel.fromJson(item)).toList();
  }

  Future<List<BillEventModel>> listEventsBill(int billId, {page = 1}) async {
    Response response = await dio
        .get("/bill/$billId/events", queryParameters: {'page': page});

    List<dynamic> json = response.data;

    return BillEventModel.buildCollection(json);
  }

  Future<BillModel> createBill(Map<String, dynamic> data) async {
    Response response = await dio.post('/bill', data: data);

    return BillModel.fromJson(response.data);
  }

  Future<BillEventModel> createBillEvent(BillModel billModel, String text) async {
    Response response = await dio.post("/bill/${billModel.id}/events", data: {'message': text});

    return BillEventModel.fromJson(response.data);
  }
}
