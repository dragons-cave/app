import 'package:dio/dio.dart';
import 'package:home_app/models/user_model.dart';
import 'package:home_app/services/base_service.dart';
import 'package:meta/meta.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';

class UserService extends BaseService {
  Future<Response> authenticate({
    @required String email,
    @required String password,
  }) async {
    Map<String, dynamic> data = {'email': email, 'password': password};

    Response response = await dio.post('/login', data: data);

    SharedPreferences prefs = await SharedPreferences.getInstance();

    await prefs.setString('currentUser', response.data["id"].toString());

    return response;
  }

  Future<Response> signUp({String email, String name, String password}) async {
    Map<String, dynamic> data = {
      'email': email,
      'password': password,
      'name': name
    };

    Response response = await dio.post('/user', data: data);

    return response;
  }

  Future<UserModel> getUser() async {
    Response response = await dio.get('/me');
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('currentUser', json.encode(response.data));
    return UserModel.fromJson(response.data);
  }

  Future<void> deleteToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.remove('token');
    await prefs.remove('currentUser');
  }

  Future<void> persistToken(String token) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('token', token);
  }

  Future<bool> hasToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('token') != null;
  }

  Future<UserModel> getUserData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Map<String, dynamic> userData = json.decode(prefs.getString('currentUser'));
    return UserModel.fromJson(userData);
  }
}
