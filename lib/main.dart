import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_app/bloc/auth_bloc.dart';
import 'package:home_app/routes.dart';

void main() => runApp(HomeApp());

ThemeData theme = ThemeData(
  primaryColor: Colors.black,
  accentColor: Colors.white,
  textTheme: TextTheme(
    title: TextStyle(
      fontSize: 32,
      fontWeight: FontWeight.bold,
    ),
    subhead: TextStyle(
      fontWeight: FontWeight.bold,
      fontSize: 18,
      color: Colors.grey.shade400,
    ),
  ),
  primaryTextTheme: TextTheme(
    button: TextStyle(
      color: Colors.white,
    ),
  ),
  cursorColor: Colors.black,
  inputDecorationTheme: InputDecorationTheme(
    labelStyle: TextStyle(
      fontWeight: FontWeight.bold,
      fontSize: 18,
      color: Colors.grey.shade500,
    ),
    errorStyle: TextStyle(
      fontWeight: FontWeight.bold,
      fontSize: 14,
      color: Colors.redAccent,
    ),
  ),
  buttonTheme: ButtonThemeData(
    buttonColor: Colors.black,
    textTheme: ButtonTextTheme.primary,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(100),
    ),
    colorScheme: ColorScheme.light(primary: Colors.black),
  ),
);

class HomeApp extends StatefulWidget {
  @override
  _HomeAppState createState() => _HomeAppState();
}

class _HomeAppState extends State<HomeApp> {
  AuthenticationBloc _authenticationBloc;

  @override
  void initState() {
    super.initState();
    _authenticationBloc = AuthenticationBloc();
    _authenticationBloc.dispatch(AppStarted());
  }

  @override
  void dispose() {
    _authenticationBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      builder: (_) => _authenticationBloc,
      child: MaterialApp(
        title: 'Home App',
        debugShowCheckedModeBanner: false,
        theme: theme,
        initialRoute: '/',
        onGenerateRoute: Router.onGenerateRoute,
      ),
    );
  }
}
