class UserModel {
  int id;
  String name;
  String email;

  UserModel.fromJson(Map<String, dynamic> json) {
    this.id = json['id'];
    this.name = json['name'];
    this.email = json['email'];
  }

  static List<UserModel> buildCollection(Iterable json) {
    return json.map((billUser) => UserModel.fromJson(billUser)).toList();
  }
}
