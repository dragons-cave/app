import 'package:home_app/models/invoice_user_model.dart';
import 'package:home_app/models/user_model.dart';

class BillUserModel {
  int id;
  String name;
  String email;
  double amount;
  double percent;
  UserModel user;
  String status;
  InvoiceUserModel lastInvoice;

  BillUserModel.fromJson(Map<String, dynamic> json) {
    this.id = json['id'];
    this.name = json['name'];
    this.email = json['email'];
    this.amount = double.parse(json['amount'].toString());
    this.percent = double.parse(json['percent'].toString());
    this.status = json['status'];
    this.lastInvoice = json['last_invoice'] != null
        ? InvoiceUserModel.fromJson(json['last_invoice'])
        : null;
  }

  static List<BillUserModel> buildCollection(Iterable json) {
    return json.map((billUser) => BillUserModel.fromJson(billUser)).toList();
  }

  String get amountLabel => "R\$ ${this.amount.toStringAsFixed(2)}";

  String get percentLabel => "${this.percent}%";
}
