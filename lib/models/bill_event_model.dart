import 'package:home_app/models/user_model.dart';
import 'package:home_app/services/user_service.dart';
import 'package:shared_preferences/shared_preferences.dart';

enum BillEventKind { message, event }

class BillEventKindHelper {
  static BillEventKind findByString(String kind) {
    return BillEventKind.values.firstWhere((BillEventKind billFrequency) {
      return !billFrequency.toString().indexOf(kind).isNegative;
    });
  }
}

class BillEventModel {
  int id;
  BillEventKind kind;
  String message;
  dynamic info;
  List<dynamic> readedBy;
  DateTime createdAt;
  UserModel user;
  bool fromCurrentUser;

  BillEventModel.fromJson(Map<String, dynamic> json) {
    this.id = json['id'];
    this.kind = BillEventKindHelper.findByString(json['kind']);
    this.message = json['message'];
    this.info = json['info'];
    this.readedBy = json['readed_by'];
    this.user = UserModel.fromJson(json['user']);
    this.createdAt = DateTime.parse(json['created_at']);
    SharedPreferences.getInstance().then((SharedPreferences prefs) {
      String userId = prefs.getString('currentUser');
      this.fromCurrentUser = this.user.id.toString() == userId;
    });
  }

  BillEventModel.fromMessage(String text) {
    UserService userService = new UserService();

    this.id = 0;
    this.kind = BillEventKind.message;
    this.message = text;
    this.info = '';
    this.readedBy = [];
    userService.getUserData().then((user) => this.user = user);
    this.createdAt = DateTime.now();
    this.fromCurrentUser = true;
  }

  static List<BillEventModel> buildCollection(Iterable json) {
    return json.map((billUser) => BillEventModel.fromJson(billUser)).toList();
  }
}
