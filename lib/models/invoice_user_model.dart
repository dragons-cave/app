import 'package:flutter/material.dart';

enum InvoiceUserStatus { available, paid, expired, canceled }

class InvoiceUserStatusHelper {
  static InvoiceUserStatus findByString(String status) {
    return InvoiceUserStatus.values
        .firstWhere((InvoiceUserStatus invoiceUserStatus) {
      return !invoiceUserStatus.toString().indexOf(status).isNegative;
    });
  }
}

class InvoiceUserModel {
  int id;
  int amount;
  String expiresAt;
  InvoiceUserStatus status;
  String userId;
  int invoiceId;
  int billUserId;
  String createdAt;
  String updatedAt;

  InvoiceUserModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    amount = json['amount'];
    expiresAt = json['expires_at'];
    status = InvoiceUserStatusHelper.findByString(json['status']);
    userId = json['user_id'];
    invoiceId = json['invoice_id'];
    billUserId = json['bill_user_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['amount'] = this.amount;
    data['expires_at'] = this.expiresAt;
    data['status'] = this.status;
    data['user_id'] = this.userId;
    data['invoice_id'] = this.invoiceId;
    data['bill_user_id'] = this.billUserId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }

  static List<InvoiceUserModel> buildCollection(Iterable json) {
    return json.map((billUser) => InvoiceUserModel.fromJson(billUser)).toList();
  }

  String get amountLabel => "R\$ ${this.amount.toStringAsFixed(2)}";

  String get statusLabel {
    switch (this.status) {
      case InvoiceUserStatus.available:
        return 'Disponivel';
      case InvoiceUserStatus.paid:
        return 'Pago';
      case InvoiceUserStatus.expired:
        return 'Atrasado';
      case InvoiceUserStatus.canceled:
        return 'Cancelado';
    }
    return '';
  }

  Color get statusColor {
    switch (this.status) {
      case InvoiceUserStatus.available:
        return Colors.blue[600];
      case InvoiceUserStatus.paid:
        return Colors.green;
      case InvoiceUserStatus.expired:
        return Colors.redAccent;
      case InvoiceUserStatus.canceled:
        return Colors.grey[350];
    }

    return Colors.grey;
  }
}
