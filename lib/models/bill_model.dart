import 'package:flutter/material.dart';
import 'package:home_app/models/bill_event_model.dart';
import 'package:home_app/models/user_model.dart';

import 'bill_user_model.dart';

enum BillStatus { pending, available, paid, expired, canceled }

class BillStatusHelper {
  static BillStatus findByString(String status) {
    return BillStatus.values.firstWhere((BillStatus billStatus) {
      return !billStatus.toString().indexOf(status).isNegative;
    });
  }
}

enum BillFrequency { day, week, month, year }

class BillFrequencyHelper {
  static BillFrequency findByString(String frequency) {
    return BillFrequency.values.firstWhere((BillFrequency billFrequency) {
      return !billFrequency.toString().indexOf(frequency).isNegative;
    });
  }

  static String getLabel(BillFrequency billFrequency, {plural = false}) {
    switch(billFrequency){
      case BillFrequency.day:
        return plural ? 'Dias' : 'Dia';
      case BillFrequency.week:
        return plural ? 'Semanas' : 'Semana';
      case BillFrequency.month:
        return plural ? 'Mêses' : 'Mês';
      case BillFrequency.year:
        return plural ? 'Anos' : 'Ano';
    }
    return '';
  }
}

class BillModel {
  int id;
  String name;
  String description;
  double amount;
  BillStatus status;
  int frequency;
  BillFrequency frequencyType;
  int invoiceId;
  UserModel owner;
  List<BillUserModel> billUsers;
  BillUserModel ownerBillUser;
  List<BillEventModel> billEvents = [];
  bool isFromOwner;

  bool hasInvoiceUsers;

  BillModel.fromJson(Map<String, dynamic> json) {
    BillStatus status = BillStatusHelper.findByString(
        json['last_invoice']['status'].toString());
    BillFrequency frequency =
        BillFrequencyHelper.findByString(json['frequency_type']);
    this.id = json['id'];
    this.name = json['name'];
    this.description = json['description'];
    this.amount = double.parse(json['amount'].toString());
    this.frequency = json['frequency'];
    this.frequencyType = frequency;
    this.status = status;
    this.invoiceId = json['last_invoice']['id'];
    this.owner = UserModel.fromJson(json['owner']);
    this.billUsers = BillUserModel.buildCollection(json['bill_users']);
    this.ownerBillUser = billUsers.firstWhere(
        (BillUserModel billUserModel) => billUserModel.id == owner.id);
    this.isFromOwner = ownerBillUser.id == owner.id;
    this.hasInvoiceUsers = billUsers.where((BillUserModel billUserModel) => billUserModel.lastInvoice != null).isNotEmpty;
  }

  String get amountLabel => "R\$ ${this.amount.toStringAsFixed(2)}";

  String get countMoreUsers => this.billUsers.length > 1
      ? "+${this.billUsers.length - 1} Pessoa${this.billUsers.length > 1 ? 's' : ''}"
      : '';

  String get userNames => billUsers.map((billUser) => billUser.name).join(', ');

  String get statusLabel {
    switch (this.status) {
      case BillStatus.pending:
        return 'Aguardando';
      case BillStatus.available:
        return 'Disponível';
      case BillStatus.paid:
        return 'Pago';
      case BillStatus.expired:
        return 'Atrasado';
      case BillStatus.canceled:
        return 'Cancelado';
    }

    return '';
  }

  Color get statusColor {
    switch (this.status) {
      case BillStatus.pending:
        return Colors.grey[600];
      case BillStatus.available:
        return Colors.blue[600];
      case BillStatus.paid:
        return Colors.greenAccent;
      case BillStatus.expired:
        return Colors.redAccent;
      case BillStatus.canceled:
        return Colors.grey[350];
    }

    return Colors.grey;
  }
}
