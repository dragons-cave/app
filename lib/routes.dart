import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_app/view/auth_view/auth_view.dart';
import 'package:home_app/view/home_view/home_view.dart';
import 'package:home_app/view/splash_view/splash_view.dart';

import 'bloc/auth_bloc.dart';

class Router {
  static Route onGenerateRoute(RouteSettings routeSettings) {
    return MaterialPageRoute(
      builder: (context) {
        return BlocBuilder(
          bloc: BlocProvider.of<AuthenticationBloc>(context),
          builder: (_, state) {
            if (state is AuthenticationAuthenticated) {
              return resolveRouteName(routeSettings.name);
            }

            if (state is AuthenticationUninitialized ||
                state is AuthenticationLoading) {
              return SplashView();
            }

            return AuthView();
          },
        );
      },
    );
  }

  static Widget resolveRouteName(routeName) {
    switch (routeName) {
      case '/':
        return HomeView();
      default:
        throw Exception('Route not founded');
    }
  }
}
