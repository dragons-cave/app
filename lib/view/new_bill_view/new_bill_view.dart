import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:home_app/bloc/bill_bloc.dart';
import 'package:home_app/models/bill_model.dart';
import 'package:intl/intl.dart' as intl;

class NewBillView extends StatefulWidget {
  @override
  _NewBillViewState createState() => _NewBillViewState();
}

class _NewBillViewState extends State<NewBillView> {
  BillBloc billBloc;
  GlobalKey<FormBuilderState> form = GlobalKey();

  MoneyMaskedTextController amountController = new MoneyMaskedTextController();

  @override
  void initState() {
    super.initState();
    billBloc = BillBloc();
  }

  String _getErrorMessage(state, input) {
    if (state is BillFailure && state.errors.containsKey(input)) {
      return state.errors[input].first;
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      bloc: billBloc,
      builder: (context, state) {
        if(state is BillCreated){
          Navigator.of(context).pop();
        }
        return FormBuilder(
          key: form,
          initialValue: {},
          child: Scaffold(
            backgroundColor: Colors.white,
            body: _getContent(state),
          ),
        );
      },
    );
  }

  Widget _getContent(BillState state) {
    Size size = MediaQuery.of(context).size;
    double padding = size.width * 0.2;
    return CustomScrollView(
      slivers: <Widget>[
        SliverAppBar(
          backgroundColor: Colors.black,
          expandedHeight: 130,
          pinned: true,
          flexibleSpace: FlexibleSpaceBar(
            centerTitle: true,
            titlePadding:
                EdgeInsets.only(bottom: 9, left: padding, right: padding),
            title: _getTitle(state),
          ),
          elevation: 0,
          leading: BackButton(
            color: Colors.white,
          ),
        ),
        AnimationLimiter(
          child: SliverList(
            delegate: SliverChildListDelegate(
              _buildContent(state),
            ),
          ),
        ),
      ],
    );
  }

  Widget _getTitle(state) {
    return FormBuilderTextField(
      attribute: 'name',
      autofocus: true,
      textAlign: TextAlign.center,
      cursorColor: Colors.white,
      textCapitalization: TextCapitalization.words,
      decoration: InputDecoration(
        hintText: 'Título',
        hintStyle: Theme.of(context).textTheme.subhead,
        border: InputBorder.none,
        errorText: _getErrorMessage(state, 'name')
      ),
      validators: [
        FormBuilderValidators.required(),
      ],
    );
  }

  List<Widget> _buildContent(state) {
    List<Widget> list = [];

    list.add(Padding(
      padding: const EdgeInsets.all(8.0),
      child: FormBuilderTextField(
        attribute: 'amount',
        controller: amountController,
        keyboardType: TextInputType.number,
        cursorColor: Colors.black,
        decoration: InputDecoration(
          labelText: 'Valor',
          prefixIcon: Icon(Icons.attach_money),
          errorText: _getErrorMessage(state, 'amount')
        ),
        validators: [
          FormBuilderValidators.required(),
          (val){              // ignore: missing_return
            double number = double.parse(val.replaceAll('.', '').replaceAll(',', '.'));
            if(number <= 1)
              return "O valor precisa ser maior do que R\$ 1,00";
          },
        ],
      ),
    ));

    list.add(Padding(
      padding: const EdgeInsets.all(8.0),
      child: FormBuilderDateTimePicker(
        firstDate: DateTime.now(),
        format: intl.DateFormat('dd/MM/yyyy'),
        attribute: 'expires_at',
        inputType: InputType.date,
        cursorColor: Colors.black,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          labelText: 'Data Final',
          prefixIcon: Icon(Icons.calendar_today),
            errorText: _getErrorMessage(state, 'expires_at')
        ),
        validators: [
          FormBuilderValidators.required(),
          FormBuilderValidators.min(1),
        ],
      ),
    ));

    list.add(Padding(
      padding: const EdgeInsets.all(8.0),
      child: FormBuilderTextField(
        attribute: 'frequency',
        cursorColor: Colors.black,
        keyboardType: TextInputType.number,
        decoration: InputDecoration(
          labelText: 'Frequência',
          prefixIcon: Icon(Icons.loop),
            errorText: _getErrorMessage(state, 'frequency')
        ),
        validators: [
          FormBuilderValidators.required(),
          FormBuilderValidators.min(1),
          FormBuilderValidators.numeric(),
        ],
      ),
    ));

    list.add(Padding(
      padding: const EdgeInsets.all(8.0),
      child: FormBuilderDropdown(
        attribute: 'frequency_type',
        items: BillFrequency.values.map((BillFrequency billFrequency) {
          return DropdownMenuItem(
            value: billFrequency,
            child: Text(
              BillFrequencyHelper.getLabel(billFrequency),
            ),
          );
        }).toList(),
        decoration: InputDecoration(
          labelText: 'Periodo',
          prefixIcon: Icon(Icons.date_range),
            errorText: _getErrorMessage(state, 'frequency_type')
        ),
        validators: [
          FormBuilderValidators.required(),
        ],
      ),
    ));

    list.add(Padding(
      padding: const EdgeInsets.all(8.0),
      child: FormBuilderTextField(
        attribute: 'description',
        textCapitalization: TextCapitalization.sentences,
        maxLines: null,
        maxLength: 150,
        cursorColor: Colors.black,
        decoration: InputDecoration(
          labelText: 'Descrição',
          prefixIcon: Icon(Icons.sort),
            errorText: _getErrorMessage(state, 'description')
        ),
        validators: [
          FormBuilderValidators.maxLength(150),
        ],
      ),
    ));

    list.add(Padding(
      padding: const EdgeInsets.symmetric(horizontal: 32.0, vertical: 8),
      child: RaisedButton(
        child: Padding(
          padding: EdgeInsets.symmetric(
            horizontal: 64,
            vertical: 15,
          ),
          child: state is BillLoading ? CircularProgressIndicator() : Text('SALVAR'),
        ),
        onPressed: () => state is BillLoading ? null : _save(),
      ),
    ));

    return list;
  }

  void _save() {
    if(form.currentState.saveAndValidate()){
      Map<String, dynamic> data = form.currentState.value;
      DateTime dateTime = data['expires_at'];
      data['expires_at'] = dateTime.toString();
      data['frequency_type'] = data['frequency_type'].toString().replaceFirst('BillFrequency.', '');

      billBloc.dispatch(CreateBillButtonPressed(data: data));
    }
  }
}
