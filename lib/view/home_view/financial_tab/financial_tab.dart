import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:home_app/bloc/financial_bloc.dart';
import 'package:home_app/models/bill_model.dart';

import 'financial_tab_item.dart';

class FinancialTab extends StatefulWidget {
  @override
  _FinancialTabState createState() => _FinancialTabState();
}

class _FinancialTabState extends State<FinancialTab> {
  final FinancialBloc financialBloc = FinancialBloc();
  List<BillModel> bills = [];

  @override
  void initState() {
    financialBloc.dispatch(FinancialTabOpened());

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      bloc: financialBloc,
      builder: (context, state) {
        if (state is FinancialLoading) {
          return _buildLoading();
        }

        if (state is FinancialLoaded) {
          if (state.bills.isEmpty) {
            return _buildEmpty();
          }

          this.bills = state.bills;

          return _buildList(context);
        }

        if (state is FinancialFailure) {
          return _buildText(state.error);
        }

        return Container();
      },
    );
  }

  Widget _buildLoading() {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        children: <Widget>[
          Center(child: CircularProgressIndicator()),
        ],
      ),
    );
  }

  Widget _buildText(String text) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0, horizontal: 24),
      child: Text(
        text,
        style: Theme.of(context)
            .textTheme
            .subhead
            .apply(color: Colors.grey.shade600),
      ),
    );
  }

  Widget _buildEmpty() {
    return _buildText('Não encontramos nenhuma conta');
  }

  Widget _buildList(context) {
    return AnimationLimiter(
      child: ListView.builder(
        itemCount: bills.length,
        itemBuilder: (BuildContext context, int index) {
          BillModel bill = bills[index];

          return AnimationConfiguration.staggeredList(
            position: index,
            duration: const Duration(milliseconds: 375),
            child: SlideAnimation(
              verticalOffset: 50.0,
              child: FadeInAnimation(
                child: FinancialTabItem(
                  bill: bill,
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
