import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_app/bloc/bill_bloc.dart';
import 'package:home_app/models/bill_model.dart';
import 'package:home_app/view/bill_view/bill_view.dart';

BorderRadius borderRadius = BorderRadius.circular(10);

class FinancialTabItem extends StatelessWidget {
  final BillModel bill;

  const FinancialTabItem({
    Key key,
    @required this.bill,
  }) : super(key: key);

  void _goToBill(context) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => BlocProvider<BillBloc>(
          builder: (context) => BillBloc(billModel: bill),
          child: BillView(),
        ),
      ),
    );
  }

  TextTheme _getTextTheme(context) {
    return Theme.of(context).textTheme;
  }

  Text _getName(context) {
    return Text(
      bill.name,
      style: _getTextTheme(context).title.apply(fontSizeDelta: -4),
    );
  }

  Text _getAmount(context) {
    return Text(
      bill.ownerBillUser.amountLabel,
      style: _getTextTheme(context).subhead.apply(color: Colors.redAccent),
    );
  }

  Text _getStatus(context) {
    return Text(
      bill.statusLabel,
      style: _getTextTheme(context).subhead.apply(color: bill.statusColor),
    );
  }

  Text _getCountPeople(context) {
    return Text(
      bill.countMoreUsers,
      style: _getTextTheme(context).subhead,
    );
  }

  Row _getFirstRow(context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        _getName(context),
        _getAmount(context),
      ],
    );
  }

  Row _getSecondRow(context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[_getStatus(context), _getCountPeople(context)],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Stack(
        children: <Widget>[
          Material(
            color: Colors.white,
            borderRadius: borderRadius,
            child: InkWell(
              borderRadius: borderRadius,
              onTap: () => _goToBill(context),
              child: Container(
                padding: EdgeInsets.all(16),
                child: Column(
                  children: <Widget>[
                    _getFirstRow(context),
                    _getSecondRow(context),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
