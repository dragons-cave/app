import 'package:flutter/material.dart';
import 'package:home_app/view/home_view/financial_tab/financial_tab.dart';
import 'package:home_app/view/new_bill_view/new_bill_view.dart';

class HomeView extends StatefulWidget {
  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  Color backgroundColor = Colors.black;
  Color tabLabelColor = Colors.white;

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        backgroundColor: backgroundColor,
        appBar: AppBar(
          elevation: 0,
          leading: IconButton(
            icon: Icon(
              Icons.menu,
              color: tabLabelColor,
            ),
            onPressed: () => {},
          ),
          actions: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: CircleAvatar(
                backgroundImage: NetworkImage(
                    'https://previews.123rf.com/images/metelsky/metelsky1809/metelsky180900233/109815470-man-avatar-profile-male-face-icon-vector-illustration-.jpg'),
              ),
            )
          ],
          backgroundColor: Colors.transparent,
          bottom: TabBar(
            isScrollable: true,
            labelStyle: Theme.of(context).textTheme.title,
            indicatorWeight: 0.0001,
            tabs: [
              Tab(text: 'Financial'),
              Tab(text: 'Home'),
              Tab(text: 'Market'),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: Colors.white,
          onPressed: () => _openByTab(),
          child: Icon(
            Icons.add,
            color: Colors.black,
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        body: TabBarView(
          children: [
            FinancialTab(),
            HomeTab(),
            MarketTab(),
          ],
        ),
      ),
    );
  }

  void _openByTab() {
    Navigator.of(context).push(MaterialPageRoute(builder: (context){
      return NewBillView();
    }));
  }
}

class HomeTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

class MarketTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
