import 'package:adhara_socket_io/adhara_socket_io.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:home_app/bloc/bill_bloc.dart';
import 'package:home_app/models/bill_event_model.dart';
import 'package:home_app/models/bill_model.dart';
import 'package:home_app/models/bill_user_model.dart';
import 'package:home_app/models/user_model.dart';
import 'package:home_app/services/base_service.dart';
import 'package:home_app/services/user_service.dart';
import 'package:home_app/view/bill_view/bill_description_view.dart';
import 'package:home_app/view/bill_view/bill_drawer.dart';
import 'package:home_app/view/bill_view/bill_event_item_view.dart';
import 'package:home_app/view/bill_view/bill_invoice_user_view.dart';
import 'package:home_app/view/bill_view/bill_message_box.dart';

class BillView extends StatefulWidget {
  @override
  _BillViewState createState() => _BillViewState();
}

class _BillViewState extends State<BillView> with TickerProviderStateMixin {
  AnimationController fabController;
  CurvedAnimation fabAnimation;
  CurvedAnimation mainFabAnimation;
  SocketIOManager socketManager;
  SocketIO socket;
  UserService userService = UserService();
  BillBloc billBloc;

  @override
  initState() {
    super.initState();

    billBloc = BlocProvider.of<BillBloc>(context);

    billBloc.dispatch(LoadBillEvents());

    fabController = new AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );

    fabAnimation = CurvedAnimation(
        parent: fabController,
        curve: Curves.fastLinearToSlowEaseIn,
        reverseCurve: Curves.fastOutSlowIn);

    mainFabAnimation = CurvedAnimation(
      parent: fabController,
      curve: Interval(
        0.5,
        0.7,
      ),
    );

//    _connectSocket();
  }

  dispose(){
    if(socketManager != null && socket != null)
      socketManager.clearInstance(socket);

    if(billBloc != null) billBloc.dispose();
    super.dispose();
  }

  _connectSocket() async {
    UserModel user  = await userService.getUserData();

    socketManager = SocketIOManager();

    socket = await socketManager.createInstance(BaseService.BASE_URL, query: {'user': user.id.toString()});

    socket.onConnect((data){
      print("connected...");
    });

    socket.on("bill_event.${billBloc.billModel.id}.new", addMessageFromSocket);

    socket.connect();
  }

  BillModel _bill() {
    return bloc().billModel;
  }

  BillBloc bloc(){
    return billBloc;
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      bloc: bloc(),
      builder: (context, BillState state) {
        return Scaffold(
          endDrawer: BillDrawerView(billModel: _bill()),
          backgroundColor: Colors.white,
          body: Stack(
            alignment: AlignmentDirectional.bottomCenter,
            children: <Widget>[
              _getContent(state),
              BillMessageBox(),
            ],
          ),
        );
      },
    );
  }

  Widget _getContent(BillState state) {
    return CustomScrollView(
      slivers: <Widget>[
        SliverAppBar(
          backgroundColor: Colors.black,
          expandedHeight: 130,
          pinned: true,
          flexibleSpace: FlexibleSpaceBar(
            centerTitle: true,
            titlePadding: EdgeInsets.only(bottom: 9),
            title: _getTitle(context),
          ),
          elevation: 0,
          leading: BackButton(
            color: Colors.white,
          ),
          actions: <Widget>[
            _getInvoicesButton(),
          ],
        ),
        AnimationLimiter(
          child: SliverList(
            delegate: SliverChildListDelegate(
              _buildContent(state),
            ),
          ),
        ),
      ],
    );
  }

  Widget _getInvoicesButton() {
    return Builder(
      builder: (context) => IconButton(
        icon: Icon(Icons.access_time),
        onPressed: () => Scaffold.of(context).openEndDrawer(),
      ),
    );
  }

  Widget _getTitle(context) {
    return Text(
      _bill().name,
      overflow: TextOverflow.ellipsis,
      style: Theme.of(context).textTheme.title.apply(color: Colors.white),
    );
  }

  List<Widget> _buildContent(BillState state) {
    Size size = MediaQuery.of(context).size;

    List<Widget> list = [];

    list.add(BillDescriptionView(
      billModel: _bill(),
      floatingActionButton: _billActions(),
    ));

    list.add(_mapInvoices(state));

    list.addAll(_mapEvents(state));

    if (state is BillEventsLoading) {
      list.add(_buildLoading());
    }

    list.add(SizedBox(
      height: size.height * 0.07,
    ));

    return list;
  }

  List<Widget> _mapEvents(BillState state) {
    if (state is BillFailure) {
      return [];
    }

    int index = -1;
    return state.billModel.billEvents.map(
      (BillEventModel billEventModel) {
        index++;
        return AnimationConfiguration.staggeredList(
          position: index,
          duration: const Duration(milliseconds: 375),
          child: SlideAnimation(
            verticalOffset: 100.0,
            child: FadeInAnimation(
              child: BillEventItem(
                billEventModel: billEventModel,
              ),
            ),
          ),
        );
      },
    ).toList();
  }

  Widget _mapInvoices(BillState state) {
    if (state is BillFailure) {
      return Container(child: Text(state.errors.values.first));
    }

    if (!_bill().hasInvoiceUsers) return Container();

    return Container(
      width: double.infinity,
      height: 150,
      color: Colors.white,
      padding: EdgeInsets.only(bottom: 9),
      child: AnimationLimiter(
        child: ListView.builder(
          itemCount: _bill().billUsers.length,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) {
            BillUserModel billUserModel = _bill().billUsers[index];
            return AnimationConfiguration.staggeredList(
              position: index,
              duration: const Duration(milliseconds: 375),
              child: SlideAnimation(
                horizontalOffset: 100.0,
                child: FadeInAnimation(
                  child: BillInvoiceUserView(
                    billUserModel: billUserModel,
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  Widget _buildLoading() {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.symmetric(vertical: 16),
      child: Center(
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(Colors.black),
        ),
      ),
    );
  }

  Widget _billActions() {
    return FloatingActionButton(
      backgroundColor: Colors.amberAccent,
      child: Icon(Icons.edit, color: Colors.black),
      onPressed: () {},
    );
  }

  void addMessageFromSocket(data) {
    print('reached');
    try {
      bloc().addEventBySocket(data);
      print('dispatched');

    }catch(e){
      print(e);
    }
  }
}
