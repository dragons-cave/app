import 'package:flutter/material.dart';
import 'package:home_app/models/bill_user_model.dart';
import 'package:home_app/models/invoice_user_model.dart';

BorderRadius borderRadius = BorderRadius.circular(12);

class BillInvoiceUserView extends StatelessWidget {
  final BillUserModel billUserModel;

  const BillInvoiceUserView({Key key, this.billUserModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    InvoiceUserModel invoiceUserModel = billUserModel.lastInvoice;

    if(invoiceUserModel == null ) return Container();

    return Padding(
      padding: const EdgeInsets.all(8),
      child: Material(
        borderRadius: borderRadius,
        color: Colors.black,
        elevation: 4,
        child: InkWell(
          borderRadius: borderRadius,
          onTap: () => {},
          child: Container(
            height: 150,
            width: 130,
            padding: EdgeInsets.all(8),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  billUserModel.name,
                  style: Theme.of(context).textTheme.subhead,
                ),
                Text(
                  invoiceUserModel.statusLabel,
                  style: Theme.of(context)
                      .textTheme
                      .subhead
                      .apply(color: invoiceUserModel.statusColor),
                ),
                Text(
                  invoiceUserModel.amountLabel,
                  style: Theme.of(context).textTheme.subhead,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
