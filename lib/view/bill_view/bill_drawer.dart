import 'package:flutter/material.dart';
import 'package:home_app/models/bill_model.dart';

class BillDrawerView extends StatelessWidget {
  final BillModel billModel;

  const BillDrawerView({Key key, this.billModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Widget> itemsDrawer = [
      Container(
        height: 190,
        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 12),
        decoration: BoxDecoration(
          color: Colors.black,
        ),
        child: DrawerHeader(
          margin: EdgeInsets.all(0.0),
          padding: EdgeInsets.all(0.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'Cobranças',
                style: Theme.of(context)
                    .textTheme
                    .title
                    .apply(color: Colors.white),
              ),
              SizedBox(
                height: 10,
              ),
              Text(
                '1 cobrança em aberto',
                style: Theme.of(context).textTheme.subhead,
              ),
              Text(
                '1 cobrança paga',
                style: Theme.of(context).textTheme.subhead,
              ),
            ],
          ),
        ),
      ),
    ];

    billModel.billUsers.forEach((user) {
      itemsDrawer.add(ListTile(
        title: Text(
          "Cobrança ${user.name}",
          style: Theme.of(context)
              .textTheme
              .subhead
              .apply(color: Colors.grey.shade500),
        ),
        onTap: () {},
      ));
    });

    return Drawer(
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: itemsDrawer,
      ),
    );
  }
}
