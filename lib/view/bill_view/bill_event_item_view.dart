import 'package:flutter/material.dart';
import 'package:home_app/models/bill_event_model.dart';

class BillEventItem extends StatelessWidget {
  final BillEventModel billEventModel;

  const BillEventItem({Key key, this.billEventModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints(minHeight: 50),
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      padding: const EdgeInsets.all(8.0),
      width: double.infinity,
      child: Row(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              height: 50,
              width: 50,
              decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), image: DecorationImage(image: NetworkImage('https://previews.123rf.com/images/metelsky/metelsky1809/metelsky180900233/109815470-man-avatar-profile-male-face-icon-vector-illustration-.jpg'))),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                billEventModel.user.name,
                style: Theme.of(context).textTheme.subhead,
              ),
              Row(
                children: <Widget>[Text(billEventModel.message)],
              )
            ],
          ),
        ],
      ),
    );
  }
}
