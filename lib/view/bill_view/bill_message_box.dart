import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_app/bloc/bill_bloc.dart';

class BillMessageBox extends StatefulWidget {
  @override
  _BillMessageBoxState createState() => _BillMessageBoxState();
}

class _BillMessageBoxState extends State<BillMessageBox> {
  TextEditingController messageInputController = TextEditingController();

  FocusNode messageFocusNode = FocusNode();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      width: size.width,
      height: size.height * 0.075,
      color: Colors.black,
      child: Row(
        children: <Widget>[
          Expanded(
            child: TextField(
              controller: messageInputController,
              focusNode: messageFocusNode,
              cursorColor: Colors.white,
              decoration: InputDecoration(
                border: InputBorder.none,
                contentPadding:
                EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                hasFloatingPlaceholder: false,
                hintText: 'Escreva uma mensagem',
                hintStyle: TextStyle(color: Colors.grey),
              ),
            ),
          ),
          IconButton(
            icon: Icon(
              Icons.send,
              color: Colors.white,
            ),
            onPressed: () => sendMessage(),
          )
        ],
      ),
    );
  }

  sendMessage() {
    String text = messageInputController.text;

    BlocProvider.of<BillBloc>(context).sendMessage(text);
    messageInputController.clear();
    messageFocusNode.unfocus();
  }
}
