import 'package:flutter/material.dart';
import 'package:home_app/models/bill_model.dart';
import 'package:home_app/models/bill_user_model.dart';

class BillDescriptionView extends StatelessWidget {
  final BillModel billModel;
  final Widget floatingActionButton;

  const BillDescriptionView({Key key, this.billModel, this.floatingActionButton}) : super(key: key);

  Widget _getSubHeader(context) {
    return Container(
      color: Colors.black,
      height: 70,
      width: double.infinity,
      child: Center(
        child: Column(
          children: <Widget>[
            Text(
              billModel.amountLabel,
              style:
                  Theme.of(context).textTheme.subhead.apply(color: Colors.grey),
            ),
            Text(
              billModel.statusLabel,
              style: Theme.of(context)
                  .textTheme
                  .subhead
                  .apply(color: billModel.statusColor),
            )
          ],
        ),
      ),
    );
  }

  Widget _getDescription(context) {
    List<Widget> description = <Widget>[
      Text(
        billModel.description,
        style: Theme.of(context).textTheme.subhead,
      ),
      SizedBox(
        height: 20,
      ),
      Text(
        "Pagadores:",
        style: Theme.of(context).textTheme.subhead,
      ),
      SizedBox(
        height: 10,
      ),
    ];

    description.addAll(_buildUserValues(context));

    return Container(
      width: double.infinity,
      decoration: BoxDecoration(color: Colors.white),
      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 32),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: description,
      ),
    );
  }

  List<Widget> _buildUserValues(context) {
    return billModel.billUsers.map((BillUserModel billUserModel) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            billUserModel.name,
            style: Theme.of(context).textTheme.subhead,
          ),
          Text(
            billUserModel.percentLabel,
            style: Theme.of(context).textTheme.subhead,
          ),
          Text(
            billUserModel.amountLabel,
            style: Theme.of(context).textTheme.subhead,
          )
        ],
      );
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          child: Column(
            children: <Widget>[
              _getSubHeader(context),
              _getDescription(context),
            ],
          ),
        ),
        Positioned(
          right: 10,
          top: 40,
          child: floatingActionButton,
        ),
      ],
    );
  }
}
