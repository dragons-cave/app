import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:home_app/bloc/auth_bloc.dart';
import 'package:home_app/bloc/sign_up_bloc.dart';
import 'package:home_app/view/auth_view/base_auth_form.dart';

class SignUpForm extends StatefulWidget {
  final Function onLogin;

  const SignUpForm({Key key, this.onLogin}) : super(key: key);

  @override
  _SignUpFormState createState() => _SignUpFormState();
}

class _SignUpFormState extends State<SignUpForm> {
  SignUpBloc signUpBloc;

  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();

  Widget textSignUp;
  Widget loadingSignUp;

  @override
  void initState() {
    signUpBloc = SignUpBloc(
      authenticationBloc: BlocProvider.of<AuthenticationBloc>(context),
    );

    textSignUp = Text('CADASTRAR');

    loadingSignUp = Container(
      height: 32,
      width: 32,
      child: CircularProgressIndicator(
        strokeWidth: 3,
      ),
    );

    super.initState();
  }

  @override
  void dispose() {
    signUpBloc.dispose();
    super.dispose();
  }

  void signUp() {
    if (_fbKey.currentState.saveAndValidate()) {
      Map<String, dynamic> form = _fbKey.currentState.value;
      signUpBloc.signUp(form['name'], form['email'], form['password']);
    }
  }

  String _getErrorMessage(state, input) {
    if (state is SignUpFailure && state.errors.containsKey(input)) {
      return state.errors[input].first;
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      bloc: signUpBloc,
      builder: (context, state) {
        return FormBuilder(
          key: _fbKey,
          initialValue: {
            'name': '',
            'email': '',
            'password': '',
            'password_confirmation': '',
          },
          child: BaseAuthForm(
            content: <Widget>[
              BaseAuthHeader(
                title: 'Cadastro',
                subhead: 'Se cadastre para começar a gerenciar seus gastos',
              ),
              FormBuilderTextField(
                attribute: 'name',
                textInputAction: TextInputAction.next,
                decoration: InputDecoration(
                    labelText: 'Nome',
                    errorText: _getErrorMessage(state, 'name')),
                validators: [
                  FormBuilderValidators.required(),
                ],
              ),
              FormBuilderTextField(
                attribute: 'email',
                textInputAction: TextInputAction.next,
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                    labelText: 'Email',
                    errorText: _getErrorMessage(state, 'email')),
                validators: [
                  FormBuilderValidators.required(),
                  FormBuilderValidators.email(),
                ],
              ),
              FormBuilderTextField(
                attribute: 'password',
                obscureText: true,
                decoration: InputDecoration(
                    labelText: 'Senha',
                    errorText: _getErrorMessage(state, 'password')),
                validators: [
                  FormBuilderValidators.required(),
                  FormBuilderValidators.minLength(6),
                ],
              ),
              FormBuilderTextField(
                attribute: 'password_confirmation',
                obscureText: true,
                decoration: InputDecoration(
                    labelText: 'Confirmação de Senha',
                    errorText: _getErrorMessage(state, 'password_confirmation')),
                validators: [
                  FormBuilderValidators.required(),
                  FormBuilderValidators.minLength(6),
                ],
              ),
              SizedBox(
                height: 30,
              ),
              RaisedButton(
                child: Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: state is SignUpLoading ? 74.5 : 64,
                    vertical: state is SignUpLoading ? 7 : 15,
                  ),
                  child: state is SignUpLoading ? loadingSignUp : textSignUp,
                ),
                onPressed: () => state is SignUpLoading ? null : signUp(),
              ),
              SizedBox(
                height: 20,
              ),
              OutlineButton(
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 81.0, vertical: 15),
                  child: Text('LOGIN'),
                ),
                onPressed: widget.onLogin,
              ),
              SizedBox(
                height: 20,
              )
            ],
          ),
        );
      },
    );
  }
}
