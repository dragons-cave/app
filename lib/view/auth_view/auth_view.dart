import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_app/bloc/auth_bloc.dart';
import 'package:home_app/view/auth_view/login_form.dart';
import 'package:home_app/view/auth_view/signup_form.dart';

class AuthView extends StatefulWidget {
  @override
  _AuthViewState createState() => _AuthViewState();
}

class _AuthViewState extends State<AuthView> with TickerProviderStateMixin {
  AnimationController loginController;
  Animation<Offset> loginOffset;
  Animation loginAnimation;

  AnimationController signUpController;
  Animation<Offset> signUpOffset;
  Animation signUpAnimation;

  Duration animationDuration;

  @override
  void initState() {
    super.initState();

    Tween baseTween = Tween<Offset>(
      begin: Offset(0.0, 10.0),
      end: Offset(0.0, 0.0),
    );

    animationDuration = Duration(milliseconds: 700);

    loginController = AnimationController(
      vsync: this,
      duration: animationDuration,
    );

    signUpController = AnimationController(
      vsync: this,
      duration: animationDuration,
    );

    loginAnimation = CurvedAnimation(
      parent: loginController,
      curve: Curves.fastLinearToSlowEaseIn,
    );

    loginOffset = baseTween.animate(loginAnimation);

    signUpAnimation = CurvedAnimation(
      parent: signUpController,
      curve: Curves.fastLinearToSlowEaseIn,
    );

    signUpOffset = baseTween.animate(signUpAnimation);

    Future.delayed(animationDuration, () => loginController.forward());
  }

  @override
  void dispose() {
    loginController.dispose();
    signUpController.dispose();
    super.dispose();
  }

  void showSignUp() {
    loginController.reverse();
    Future.delayed(animationDuration, () => signUpController.forward());
  }

  void showLogin() {
    signUpController.reverse();
    Future.delayed(animationDuration, () => loginController.forward());
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      builder: (context) => BlocProvider.of<AuthenticationBloc>(context),
      child: Scaffold(
        backgroundColor: Theme.of(context).primaryColor,
        body: SingleChildScrollView(
          child: Stack(
            alignment: AlignmentDirectional.bottomCenter,
            children: <Widget>[
              AuthBackground(),
              SlideTransition(
                position: loginOffset,
                child: LoginForm(onSignUp: showSignUp),
              ),
              SlideTransition(
                position: signUpOffset,
                child: SignUpForm(onLogin: showLogin),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class AuthBackground extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;

    return Container(
      height: height,
      width: double.infinity,
      child: SafeArea(
        child: Column(
          children: <Widget>[
            Container(
              height: height * 0.20,
              child: Image.network(
                'https://icon-library.net/images/home-logo-icon/home-logo-icon-0.jpg',
                color: Colors.white,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
