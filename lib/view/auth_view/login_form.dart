import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:home_app/bloc/auth_bloc.dart';
import 'package:home_app/bloc/login_bloc.dart';
import 'package:home_app/view/auth_view/base_auth_form.dart';

class LoginForm extends StatefulWidget {
  final Function onSignUp;

  const LoginForm({Key key, this.onSignUp}) : super(key: key);

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  LoginBloc loginBloc;

  final GlobalKey<FormBuilderState> _fbKey = GlobalKey<FormBuilderState>();

  Widget textLogin;
  Widget loadingLogin;

  Map<String, List> errors = {};

  @override
  void initState() {
    loginBloc = LoginBloc(
        authenticationBloc: BlocProvider.of<AuthenticationBloc>(context));

    textLogin = Text('ENTRAR');

    loadingLogin = Container(
      height: 32,
      width: 32,
      child: CircularProgressIndicator(
        strokeWidth: 3,
      ),
    );
    super.initState();
  }

  @override
  void dispose() {
    loginBloc.dispose();
    super.dispose();
  }

  void login() {
    if (_fbKey.currentState.saveAndValidate()) {
      Map<String, dynamic> form = _fbKey.currentState.value;
      loginBloc.login(form['email'], form['password']);
    }
  }

  String _getErrorMessage(state, input) {
    if (state is LoginFailure && state.errors.containsKey(input)) {
      return state.errors[input].first;
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
      bloc: loginBloc,
      builder: (context, state) {
        bool isLoading = state is LoginLoading;
        return FormBuilder(
          key: _fbKey,
          initialValue: {
            'email': '',
            'password': '',
          },
          child: BaseAuthForm(
            content: <Widget>[
              BaseAuthHeader(
                title: 'Bem vindo',
                subhead: 'Entre para acessar seus gastos',
              ),
              FormBuilderTextField(
                attribute: 'email',
                textInputAction: TextInputAction.next,
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                    labelText: 'Email',
                    errorText: _getErrorMessage(state, 'email')),
                validators: [
                  FormBuilderValidators.required(),
                  FormBuilderValidators.email(),
                ],
              ),
              FormBuilderTextField(
                attribute: 'password',
                obscureText: true,
                decoration: InputDecoration(
                    labelText: 'Senha',
                    errorText: _getErrorMessage(state, 'password')),
                validators: [
                  FormBuilderValidators.required(),
                  FormBuilderValidators.minLength(6),
                ],
              ),
              SizedBox(
                height: 30,
              ),
              RaisedButton(
                child: Padding(
                  padding: EdgeInsets.symmetric(
                    horizontal: isLoading ? 74.5 : 64,
                    vertical: isLoading ? 7 : 15,
                  ),
                  child: isLoading ? loadingLogin : textLogin,
                ),
                onPressed: () => isLoading ? null : login(),
              ),
              SizedBox(
                height: 20,
              ),
              OutlineButton(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 52.0, vertical: 15),
                  child: Text('CADASTRO'),
                ),
                onPressed: widget.onSignUp,
              ),
            ],
          ),
        );
      },
    );
  }
}
