import 'package:flutter/material.dart';

class BaseAuthForm extends StatelessWidget {
  final List<Widget> content;

  const BaseAuthForm({Key key, this.content}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 8.0),
      child: Column(
        children: <Widget>[
          SizedBox(
            height: height * 0.2,
          ),
          ConstrainedBox(
            constraints: BoxConstraints(minHeight: height * 0.7),
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(50),
                  topRight: Radius.circular(50),
                ),
              ),
              child: Container(
                padding: EdgeInsets.only(
                  top: 42,
                  left: 32,
                  right: 32,
                  bottom: 32,
                ),
                child: Column(
                  children: content,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class BaseAuthHeader extends StatelessWidget {
  final String title;
  final String subhead;

  const BaseAuthHeader({Key key, this.title, this.subhead}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            title,
            style: Theme.of(context).textTheme.title,
          ),
          SizedBox(
            height: 7,
          ),
          Text(
            subhead,
            style: Theme.of(context).textTheme.subhead,
          )
        ],
      ),
    );
  }
}
